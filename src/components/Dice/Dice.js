import styles from "./Dice.module.css";

import one from "./images/one.png";
import two from "./images/two.png";
import three from "./images/three.png";
import four from "./images/four.png";
import five from "./images/five.png";
import six from "./images/six.png";

export default function Dice({num}) {
  const images = [one, two, three, four, five, six];
  return (
    <div className={styles.dice}>
      <img src={images[num - 1]} alt={num} />
    </div>
  );
}