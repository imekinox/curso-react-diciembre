import { useState } from "react";
import Button from "../Button/Button";
import Dice from "../Dice/Dice";
import styles from "./DiceGame.module.css";

export default function DiceGame() {
  // numero de dados
  const [numberOfDice, setNumberOfDice] = useState(null);
  // los numeros que salieron de cada dado
  const [roll, setRoll] = useState([]);
  // sumatoria de los dados
  const [sumOfDice, setSumOfDice] = useState(0);

  // funcion que tire los dados
  // se va a ejecutar cuando se haga click
  // en alguno de los botones
  const tirarDados = (numero) => {
    // depende la cantidad de dados
    // hay un bucle donde calcule un numero aleatorio
    // entre 1 y 6
    let rolls = [];
    let sumOfDice = 0;
    for (let index = 0; index < numero; index++) {
      rolls[index] = Math.floor(Math.random() * 6) + 1;
      // sumOfDice = sumOfDice + rolls[index];
      sumOfDice += rolls[index];
    }
    // console.log("rolls", rolls);
    // console.log("sumOfDice", sumOfDice);
    setRoll(rolls);
    setSumOfDice(sumOfDice);
    setNumberOfDice(numero);
  };

  return (
    <div className="App">
      <h1>Dice Roller</h1>
       {/* {[1, 2, 3, 4, 5].map((a, i) => {
        return (
          <Button
            // iterador unico por cada vuelta del map
            key={i}
            // en cada vuelta del map
            //le pasamos al componente Button el elemento a
            // que cada vuelta va a ser 1, 2, 3, 4 o 5
            numeroDelBoton={a}
            handleClick={(t) => tirarDados(t)}
          />
        );
      })} */}
      <div className={styles.centeredRow}>
        {[1, 2, 3, 4, 5].map((value) => {
          return <Button
            key={value}
            numeroDelBoton={value} 
            handleClick={tirarDados} />
        })}
      </div>
      <div>RESULTADO RENDERIZADO DADOS</div>
      <div className={styles.centeredRow}>
        {roll.map((num, i) => <Dice key={i} num={num} />)}
      </div>
      {/* dependiendo del array rolls, mostrar las imagenes
      de los dados que correspondan
      hacemos un map del array renderizando la imagen correspondiente */}
      <div>RESULTADO EN NUMEROS</div>
      SE SUMO: {sumOfDice} DE UN TOTAL POSIBLE: {numberOfDice * 6}
    </div>
  );
}
